# translation of kirigami-addons.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kirigami-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-16 00:44+0000\n"
"PO-Revision-Date: 2022-04-15 19:10+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: components/AbstractMaximizeComponent.qml:126
#, kde-format
msgid "Close"
msgstr ""

#: components/AlbumMaximizeComponent.qml:112
#, kde-format
msgid "Zoom in"
msgstr ""

#: components/AlbumMaximizeComponent.qml:117
#, kde-format
msgid "Zoom out"
msgstr ""

#: components/AlbumMaximizeComponent.qml:123
#, kde-format
msgid "Rotate left"
msgstr ""

#: components/AlbumMaximizeComponent.qml:129
#, kde-format
msgid "Rotate right"
msgstr ""

#: components/AlbumMaximizeComponent.qml:134
#, kde-format
msgctxt "@action:intoolbar"
msgid "Show caption"
msgstr ""

#: components/AlbumMaximizeComponent.qml:134
#, kde-format
msgctxt "@action:intoolbar"
msgid "Hide caption"
msgstr ""

#: components/AlbumMaximizeComponent.qml:140
#, kde-format
msgid "Save as"
msgstr ""

#: components/AlbumMaximizeComponent.qml:200
#, fuzzy, kde-format
#| msgid "Previous"
msgid "Previous image"
msgstr "Predchádzajúci"

#: components/AlbumMaximizeComponent.qml:218
#, kde-format
msgid "Next image"
msgstr ""

#: components/Banner.qml:186
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr ""

#: components/DownloadAction.qml:37
#, kde-format
msgid "Download"
msgstr ""

#: components/VideoMaximizeDelegate.qml:233
#, kde-format
msgctxt "@action:button"
msgid "Volume"
msgstr ""

#: dateandtime/DatePicker.qml:141
#, kde-format
msgctxt "%1 is month name, %2 is year"
msgid "%1 %2"
msgstr "%1 %2"

#: dateandtime/DatePicker.qml:182
#, fuzzy, kde-format
#| msgid "Days"
msgctxt "@title:tab"
msgid "Days"
msgstr "Dni"

#: dateandtime/DatePicker.qml:190
#, fuzzy, kde-format
#| msgid "Months"
msgctxt "@title:tab"
msgid "Months"
msgstr "Mesiace"

#: dateandtime/DatePicker.qml:196
#, fuzzy, kde-format
#| msgid "Years"
msgctxt "@title:tab"
msgid "Years"
msgstr "Roky"

#: dateandtime/DatePopup.qml:55
#, kde-format
msgid "Previous"
msgstr "Predchádzajúci"

#: dateandtime/DatePopup.qml:64
#, kde-format
msgid "Today"
msgstr "Dnes"

#: dateandtime/DatePopup.qml:73
#, kde-format
msgid "Next"
msgstr "Nasledujúci"

#: dateandtime/DatePopup.qml:92
#, kde-format
msgid "Cancel"
msgstr "Zrušiť"

#: dateandtime/DatePopup.qml:101
#, kde-format
msgid "Accept"
msgstr "Prijať"

#: dateandtime/private/TumblerTimePicker.qml:88
#, kde-format
msgctxt "Time separator"
msgid ":"
msgstr ""

#: formcard/AboutKDE.qml:23 formcard/labs/AboutKDE.qml:28
#, kde-format
msgid "About KDE"
msgstr ""

#: formcard/AboutKDE.qml:50 formcard/labs/AboutKDE.qml:60
#, kde-format
msgid "KDE"
msgstr ""

#: formcard/AboutKDE.qml:59 formcard/labs/AboutKDE.qml:69
#, kde-format
msgid "Be Free!"
msgstr ""

#: formcard/AboutKDE.qml:68 formcard/labs/AboutKDE.qml:78
#, kde-format
msgid ""
"KDE is a world-wide community of software engineers, artists, writers, "
"translators and creators who are committed to Free Software development. KDE "
"produces the Plasma desktop environment, hundreds of applications, and the "
"many software libraries that support them.\n"
"\n"
"KDE is a cooperative enterprise: no single entity controls its direction or "
"products. Instead, we work together to achieve the common goal of building "
"the world's finest Free Software. Everyone is welcome to join and contribute "
"to KDE, including you."
msgstr ""

#: formcard/AboutKDE.qml:76 formcard/AboutPage.qml:179
#: formcard/labs/AboutKDE.qml:86 formcard/labs/AboutPage.qml:194
#, kde-format
msgid "Homepage"
msgstr ""

#: formcard/AboutKDE.qml:82
#, kde-format
msgid "Report bugs"
msgstr ""

#: formcard/AboutKDE.qml:87 formcard/labs/AboutKDE.qml:99
#, kde-format
msgid ""
"Software can always be improved, and the KDE team is ready to do so. "
"However, you - the user - must tell us when something does not work as "
"expected or could be done better.\n"
"\n"
"KDE has a bug tracking system. Use the button below to file a bug, or use "
"the program's About page to report a bug specific to this application.\n"
"\n"
"If you have a suggestion for improvement then you are welcome to use the bug "
"tracking system to register your wish. Make sure you use the severity called "
"\"Wishlist\"."
msgstr ""

#: formcard/AboutKDE.qml:96 formcard/AboutPage.qml:230
#: formcard/labs/AboutKDE.qml:108 formcard/labs/AboutPage.qml:248
#, kde-format
msgid "Report a bug"
msgstr ""

#: formcard/AboutKDE.qml:102
#, kde-format
msgid "Join us"
msgstr ""

#: formcard/AboutKDE.qml:107 formcard/labs/AboutKDE.qml:121
#, kde-format
msgid ""
"You do not have to be a software developer to be a member of the KDE team. "
"You can join the national teams that translate program interfaces. You can "
"provide graphics, themes, sounds, and improved documentation. You decide!"
msgstr ""

#: formcard/AboutKDE.qml:115 formcard/AboutPage.qml:205
#: formcard/labs/AboutKDE.qml:128 formcard/labs/AboutPage.qml:222
#, kde-format
msgid "Get Involved"
msgstr ""

#: formcard/AboutKDE.qml:123 formcard/labs/AboutKDE.qml:135
#, kde-format
msgid "Developer Documentation"
msgstr ""

#: formcard/AboutKDE.qml:129
#, kde-format
msgid "Support us"
msgstr ""

#: formcard/AboutKDE.qml:134 formcard/labs/AboutKDE.qml:148
#, kde-format
msgid ""
"KDE software is and will always be available free of charge, however "
"creating it is not free.\n"
"\n"
"To support development the KDE community has formed the KDE e.V., a non-"
"profit organization legally founded in Germany. KDE e.V. represents the KDE "
"community in legal and financial matters.\n"
"\n"
"KDE benefits from many kinds of contributions, including financial. We use "
"the funds to reimburse members and others for expenses they incur when "
"contributing. Further funds are used for legal support and organizing "
"conferences and meetings.\n"
"\n"
"We would like to encourage you to support our efforts with a financial "
"donation.\n"
"\n"
"Thank you very much in advance for your support."
msgstr ""

#: formcard/AboutKDE.qml:146 formcard/labs/AboutKDE.qml:159
#, kde-format
msgid "KDE e.V"
msgstr ""

#: formcard/AboutKDE.qml:154 formcard/AboutPage.qml:192
#: formcard/labs/AboutKDE.qml:166 formcard/labs/AboutPage.qml:208
#, kde-format
msgid "Donate"
msgstr ""

#: formcard/AboutPage.qml:87 formcard/labs/AboutPage.qml:96
#, kde-format
msgid "About %1"
msgstr ""

#: formcard/AboutPage.qml:133 formcard/labs/AboutPage.qml:148
#, kde-format
msgid "Copyright"
msgstr ""

#: formcard/AboutPage.qml:141 formcard/labs/AboutPage.qml:155
#, kde-format
msgid "License"
msgid_plural "Licenses"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: formcard/AboutPage.qml:237 formcard/labs/AboutPage.qml:261
#, kde-format
msgid "Libraries in use"
msgstr ""

#: formcard/AboutPage.qml:260 formcard/labs/AboutPage.qml:289
#, kde-format
msgid "Authors"
msgstr ""

#: formcard/AboutPage.qml:275 formcard/labs/AboutPage.qml:301
#, kde-format
msgid "Credits"
msgstr ""

#: formcard/AboutPage.qml:290 formcard/labs/AboutPage.qml:313
#, kde-format
msgid "Translators"
msgstr ""

#: formcard/AboutPage.qml:359
#, kde-format
msgid "Visit %1's KDE Store page"
msgstr ""

#: formcard/AboutPage.qml:368
#, kde-format
msgid "Send an email to %1"
msgstr ""

#: settings/CategorizedSettings.qml:44
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr ""

#: settings/CategorizedSettings.qml:44
#, kde-format
msgctxt "@title:window"
msgid "Settings — %1"
msgstr ""
